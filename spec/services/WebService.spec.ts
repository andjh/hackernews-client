/**
 * WebService Jasmine Spec
 */

import WebService from '../../src/assets/js/service/WebService';
import mock from 'xhr-mock';


describe("WebService", () => {

    let Module:any = {};
    let originalTimeout:any;

    afterEach(() => mock.teardown());

    beforeEach(() => {
        mock.setup()
        Module = new WebService();
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    });

    it("should return a list of top stories", async (done) => {

        const url = "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty";

        mock.get(url, (req, res) => {
            return res.status(200).body('[213213,231231,23232]');
        });

        await Module.get(url).then((resp:any) => {
            expect(resp instanceof Array).toBe(true);
            expect(Object.keys(resp)).not.toBeLessThan(0);
            done();
        });


    });

    it("should return contents of a story", async (done) => {

        const url = "https://hacker-news.firebaseio.com/v0/item/160705.json?print=pretty";

        mock.get(url, (req, res) => {
            return res.status(200).body('{ "by" : "pg", "id" : 160705, "poll" : 160704, "score" : 335, "text" : "Yes, ban them; I\'m tired of seeing Valleywag stories on News.YC.", "time" : 1207886576, "type" : "pollopt" }');
        });

        await Module.get(url).then((resp:any) => {
            expect(Object.keys(resp)).not.toBeLessThan(0);
            expect(resp["id"]).toEqual(160705);
            done();
        });

    });

});
