/**
 * Feed Jasmine Spec
 */

import Feed from '../../src/assets/js/model/Feed';
import mock from 'xhr-mock';

describe("Repository", () => {

    let Module:any = {};

    afterEach(() => mock.teardown());

    beforeEach(() => {
        mock.setup()
        Module = new Feed();
    });

    it("should return a populated list of top stories", async (done) => {

        const url = "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty";
        mock.get(url, (req, res) => {
            return res.status(200).body('[2,160704]');
        });

        const items = [',','2','3','1','160704'];

        items.forEach((item) => {
            const url = `https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`;
            mock.get(url, (req, res) => {
                return res.status(200).body('{ "by" : "pg", "id" : 160705, "poll" : 160704, "score" : 335, "text" : "Yes, ban them; I\'m tired of seeing Valleywag stories on News.YC.", "time" : 1207886576, "type" : "pollopt" }');
            });
        });

        await Module.getTopStories(2).then((resp:any) => {
            expect(resp instanceof Object).toBe(true);
            done();
        });

    });


});
