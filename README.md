# HackerNews Client #

Technical Test for AND Digital

Author: Jabari Holder

Time: ~2 hours

## Overview

This application functions by displaying a feed list of news.

## Installation

Requirements:

- Node v6.11+
- NPM v3.10+

After cloning the repository, run the following command via terminal:
```
cd /to/clone/directory/
npm i
```

## Getting Started

### Basic / Development

To start the application, run the following in a terminal window:
```
npm start
```

### Testing and Linting

Run the following to test and lint the application:
```
npm run test
npm run lint
```

### Building

Run the following for production builds:
```
npm run build
```

## Potential Future Improvements

- Allow cache to only update stories it's missing
- Addition of more unit tests along with integration and end-to-end tests
- Add remainder of functionality (hide, view comments, pagination, etc.)
- Better time conversion
- Better dependency injection and testing
- Better error handling (improved UX so the user is aware of issues)
- Better templating system
- Definition of browsers supported and the test that run against it


## License

MIT License
