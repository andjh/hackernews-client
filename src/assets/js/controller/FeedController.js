/**
 * ===============================================================
 * Feed Controller
 * @desc Controls the feed list view depending on the given model
 * @author Jabari Holder
 * ===============================================================
 */

/** Dependencies */
import Feed from '../model/Feed';

 /**
  * Creates a new Web Service
  * @class
  */
export default class FeedController {
    /**
     * Loaded on class instantiation
     * @param {Object} feed Application repo
     * @constructor
     */
    constructor(feed = new Feed()) {
        this.feed = feed;
        this.populateFeedList();
    }

    /**
     * Update the feed list with data
     */
    populateFeedList() {
        let feedList = document.getElementById('feed-list');
        feedList.innerHTML = '';
        this.feed.getTopStories(10).then((stories) => {
            for (let i in stories) {
                if (stories.hasOwnProperty(i) && stories[i] !== null) {
                    const template = this.template('feedlist', stories[i]);
                    const node = this.templateToHTML(template);
                    feedList.appendChild(node);
                }
            }
        }).catch((error) => {
            console.error(error);
        });
    }

    /**
     * Create HTML node from HTML text
     * @param  {String} htmlText HTML string
     * @return {Object}          HTML nodes
     */
    templateToHTML(htmlText) {
        let node = document.createElement('div');
        node.innerHTML = htmlText.trim();
        return node.firstChild;
    }

    /**
     * Converts date to human readable fonrmat
     * @param  {Integer} seconds Date in seconds
     * @return {string}      date
     */
    dateToHuman(seconds) {
        let date = new Date(seconds);
        return date.getUTCHours()+' hours';
    }

    /**
     * Get a templates
     * @param  {String} type Name of the templates
     * @param  {Object} data Template data variables
     * @return {String}      Template data
     */
    template(type, data) {
        let templates = {
            feedlist: (data) => {
                return `
                <li>
                    <div class="upvote">
                        <a href="#" title="upvote">
                            <div class="arrow"></div>
                        </a>
                    </div>

                    <div class="item-info">
                        <div class="title">
                            <a href="#" title="${data.title}">
                                <h1>${data.title}</h1>
                            </a>
                            <a href="#" title="${data.url}">
                                <span class="source">
                                    (${new URL(data.url).hostname})
                                </span>
                            </a>
                        </div>
                        <div class="metadata">
                            <div class="points">
                                ${data.score} points by
                                <a href="#">${data.by}</a>
                            </div>
                            <div class="time">
                                <a href="#">
                                    ${this.dateToHuman(data.time)} ago
                                </a>
                            </div>
                            <div class="separator">|</div>
                            <div class="hide">
                                <a href="#" title="hide">hide</a>
                            </div>
                            <div class="separator">|</div>
                            <div class="comments">
                                <a href="#">${data.kids.length} comments</a>
                            </div>
                        </div>
                    </div>
                </li>
                `;
            },
        };
        if (templates.hasOwnProperty(type)) return templates[type](data);
    }
}
