/**
 * ===============================================================
 * Repository
 * @desc Retrieves data depending on where it resides
 * @author Jabari Holder
 * ===============================================================
 */

/** Dependencies */
const localStorage = require('localStorage');
import WebService from '../service/WebService';

 /**
  * Creates a new Web Service
  * @class
  */
export default class Repository {
    /**
     * Loaded on class instantiation
     * @param {Object} webService Service to access API endpoints
     * @constructor
     */
    constructor(webService = new WebService()) {
        this.webService = webService;
        this.config = {
            url: {
                structure: {
                    base: 'https://hacker-news.firebaseio.com/v0/',
                    end: '.json?print=pretty',
                },
                validEndpoints: ['topstories', 'item'],
            },
        };
    }
    /**
     * Get repository infor
     * @param  {String} item Name of item to get
     * @param  {String} ext  Extensions of Item
     * @return {Promise}      Promise
     */
    get(item = '', ext = '') {
        return new Promise((resolve, reject) => {
            const data = this.getCacheData(item);
            if (typeof data === undefined || Object.keys(data).length < 1) {
                this.getAPIData(item, ext).then((data)=> {
                    resolve(data);
                }).catch((error)=>{
                    reject(error);
                });
            } else {
                resolve(data);
            }
        });
    }

    /**
     * Get API data
     * @param  {String} item Item key
     * @param  {String} ext Item key
     * @return {Promise}     Promise of API data
     * @method
     */
    getAPIData(item = '', ext = '') {
        return new Promise((resolve, reject) => {
            const url = this.buildURL(item, ext);
            if (!url) return reject('Please enter a valid item name');

            this.webService.get(url).then((data) => {
                this.setCacheData(item.concat(ext), data);
                resolve(data);
            }).catch((error)=> {
                reject(error);
            });
        });
    }

    /**
     * Build URL from Partials
     * @param  {String} type Type of URL to build
     * @param  {String} ext  Additional extension params
     * @return {String}     Built URL
     */
    buildURL(type = '', ext = '') {
        if (this.config.url.validEndpoints.indexOf(type) < 0) return false;

        const structure = this.config.url.structure;
        let baseURL = structure.base.concat(type);

        if (ext) baseURL = baseURL.concat('/').concat(ext);
        return baseURL.concat(structure.end);
    }

    /**
     * Get cache for data
     * @param  {String} item Item key
     * @return {Object}      Cached data
     * @method
     */
    getCacheData(item = '') {
        const cachedData = localStorage.getItem(item);
        if (cachedData !== null && cachedData) {
             return JSON.parse(cachedData);
        }
        return {};
    }

    /**
     * Set cache for data
     * @param  {String} item Item key
     * @param  {Object} data Item key
     * @method
     */
    setCacheData(item = '', data = {}) {
        localStorage.setItem(item, JSON.stringify(data));
    }
}
