/**
 * ===============================================================
 * Feed Model
 * @desc Create the model of a Feed for the controller to use
 * @author Jabari Holder
 * ===============================================================
 */

/** Dependencies */
import Repository from '../repository/Repository';

 /**
  * Creates a new Web Service
  * @class
  */
export default class Feed {
    /**
     * Loaded on class instantiation
     * @param {Object} repository Application repo
     * @constructor
     */
    constructor(repository = new Repository()) {
        this.repository = repository;
    }

    /**
     * Get a list of top stories
     * @param  {Number} [amount=10] Amount of top stories
     * @return {Promise}            List of top stories
     */
    getTopStories(amount = 10) {
        let promiseList = [];
        return new Promise((resolve, reject) => {
            this.repository.get('topstories').then((stories) => {
                for (let i in stories) {
                    if (stories.hasOwnProperty(i)) {
                        if (i > (amount-1)) break;
                        let promise = this.repository.get('item', stories[i]);
                        promiseList.push(promise);
                    }
                }
                Promise.all(promiseList).then((values)=> {
                    resolve(values);
                }).catch((error) => {
                    console.log(error);
                });
            });
        });
    }
}
