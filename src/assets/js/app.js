/**
 * ========================================
 * TECHNICAL TEST
 * @author Jabari Holder
 * =======================================
 *
 */

/** Dependenceis */
import FeedController from './controller/FeedController';

/**
 * Launch the main application
 * @class
 */
class App {
    /**
     * Launch on instanciation
     * @param {Object} [feedController] Controlls main feed
     */
    constructor(feedController = new FeedController()) {
        this.feedController = feedController;
    }
}

new App();
if (module.hot) module.hot.accept();
