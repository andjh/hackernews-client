/**
 * ===============================================================
 * WebService
 * @desc Serivce for interactiong with API endpoints
 * @author Jabari Holder
 * ===============================================================
 */

 /**
  * Creates a new Web Service
  * @class
  */
export default class WebService {
    /**
     * Get request
     * @method get
     * @param  {String} url URL to request endpoint
     * @return {Promise}    Promise of response
     */
    get(url = '') {
        return new Promise((resolve, reject) => {
            this.request(url).then((response) => {
                resolve(response);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Request data from url
     * @param  {String} url Path to request data from
     * @return {Promise}    Response as a promise
     */
    request(url = '') {
        return new Promise((resolve, reject) => {
            const xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = () => {
                if (xhttp.readyState == 4) {
                    const respJSON = this.responseToJSON(xhttp.responseText);
                    if (xhttp.status == 200) {
                        resolve(respJSON);
                    } else {
                        reject(respJSON);
                    }
                }
            };

            xhttp.open('GET', url, true);
            xhttp.send();
        });
    }

    /**
     * Convert response text to json
     * @param  {String} responseText [description]
     * @return {[type]}               [description]
     */
    responseToJSON(responseText = '') {
        return JSON.parse(responseText);
    }
}
