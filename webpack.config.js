const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = env => {
    const public_path = (env.production ? '/' : '/assets/');

    return {
        target: "web",
        mode: "development",
        entry: ['./src/assets/js/app.js', './src/views/index.html'],
        output: {
            path: path.resolve(__dirname, 'dist/'),
            filename: 'assets/app.bundle.js'
        },
        plugins: [
            new CopyWebpackPlugin([
                { from: './src/views/index.html', to: 'index.html' },
                { from: './src/assets/css/', to: 'assets/css/' },
                { from: './src/assets/img/', to: 'assets/img/' },
            ]),
            //new CleanWebpackPlugin(['./dist/']),
            new webpack.HotModuleReplacementPlugin()
        ],
        module: {
            rules: [
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    query: {
                        presets: ['es2015']
                    }
                },
                {
                  test: /\.(html)$/,
                  use: {
                    loader: 'html-loader',
                    options: {
                      attrs: [':data-src']
                    }
                  }
                }
            ]
        },
        stats: {
           entrypoints: false,
           children: false
        },
        resolve: {
            extensions: [".js"]
        },
        devServer: {
            publicPath: public_path,
            contentBase: path.resolve(__dirname, "dist"),
            watchContentBase: true,
            host: 'localhost',
            port: 8000,
            compress: true,
            hot: true,
            open: 'Google Chrome',
            overlay: true,
            inline: true
        }
    }
};
